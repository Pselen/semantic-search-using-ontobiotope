# Semantic Search Using Ontologies

To run the project, download the repository and install `requirements.txt` and then run `runner.py` from the **directory you have downloaded the repo**. To run your own queries without training the model, you can comment-out `train` and `test` lines in `runner.py` and change the query string according to your choice. The outputs will be given in two different files named `taxonomy_results.txt` and `cooccurence_results.txt` under `data` folder.

All the paths are already adjusted in the `configs.json`. Note that for code the run, you would need pre-trained word embeddings, which is a large file which are not allowed to share publicly. Thus, you need to find us @BM36 :)

Enjoy!